﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homeworkserialize
{
    internal interface ISerialize
    {
        public string Serialize<T>(T obj,string delimiter=",");
        public T DeSerialize<T>(string obj, string delimiter = ",") where T : class, new();
    }
}
