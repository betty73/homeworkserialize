﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace homeworkserialize
{
    public class CsvSerialize:ISerialize
    {
        public string Serialize<T>(T obj, string delimiter = ",")
        {
            if (obj == null) throw new ArgumentNullException();
            var sb = new StringBuilder();
            var properties = getProperties(obj);
            var data = new List<string>();
            foreach (var prop in properties)
            {
                var propValue = prop.GetValue(obj);
                data.Add(propValue == null ? "" : propValue.ToString());
            }
            return sb.Append(string.Join(delimiter,data)).ToString();

        }
        public T DeSerialize<T>(string obj, string delimiter = ",") where T : class, new()
        {
            if (obj == null) throw new ArgumentNullException();
            var values = obj.Split(delimiter);
            var data = new T();
            var properties = getProperties(data);
            var i = 0;
            foreach (var prop in properties)
            {               
                prop.SetValue(data,
                    Convert.ChangeType(values[i++], prop.PropertyType), null);
            }
            return data;

        }
        private PropertyInfo[] getProperties<T>(T obj)
        {
            return typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty).ToArray();
        }
    }
}
