﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using Newtonsoft.Json;

namespace homeworkserialize
{
    public class TheEasiestBenchmark
    {
        TestClass testObject = new TestClass() { I1 = 1, I2 = 2, I3 = 3, I4 = 4, I5 = 5 };
        private static ISerialize serializer = new CsvSerialize();
        [Benchmark(Description = "Serialize to Csv", OperationsPerInvoke = 2)]
        public void SerializeWithCustomCsv()
        {
            serializer.Serialize<TestClass>(testObject);
        }

        [Benchmark(Description = "Deserialize from Csv")]
        public void DeserializeWithCustomCsv()
        {
            testObject = serializer.DeSerialize<TestClass>("1,2,3,4,5");
        }


        [Benchmark(Description = "Serialize to NewtonsoftJson")]
        public void SerializeWithNewtonsoftJson()
        {
            JsonConvert.SerializeObject(testObject);
        }

        [Benchmark(Description = "Deserialize from NewtonsoftJson")]
        public void DeserializeWithNewtonsoftJson()
        {
            testObject = JsonConvert.DeserializeObject<TestClass>("{\"I1\":1,\"I2\":2,\"I3\":3,\"I4\":4,\"I5\":5}");
        }

        [Benchmark(Description = "Serialize to SystemTextJson")]
        public void SerializeWithSystemTextJson()
        {
            System.Text.Json.JsonSerializer.Serialize(testObject);
        }

        [Benchmark(Description = "Deserialize from SystemTextJson")]
        public void DeserializeWithSystemTextJson()
        {
            testObject = JsonConvert.DeserializeObject<TestClass>("{\"I1\":1,\"I2\":2,\"I3\":3,\"I4\":4,\"I5\":5}");
        }

    }
}
