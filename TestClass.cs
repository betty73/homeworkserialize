﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homeworkserialize
{
    public class TestClass
    {
        public int I1 { get; set; }
        public int I2 { get; set; }
        public int I3 { get; set; }
        public int I4 { get; set; }
        public int I5 { get; set; }
        public override string ToString()
        {
            return $"I1:{I1} I2:{I2} I3:{I3} I4:{I4} I5:{I5}";

        }
    }
}
