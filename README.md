BenchmarkDotNet=v0.13.1, OS=Windows 10.0.19044.1645 (21H2)
11th Gen Intel Core i5-11400 2.60GHz, 1 CPU, 12 logical and 6 physical cores
.NET SDK=6.0.400-preview.22301.10
  [Host]     : .NET 6.0.5 (6.0.522.21309), X64 RyuJIT  [AttachedDebugger]
  DefaultJob : .NET 6.0.5 (6.0.522.21309), X64 RyuJIT


|                            Method |       Mean |    Error |   StdDev |
|---------------------------------- |-----------:|---------:|---------:|
|                'Serialize to Csv' |   273.9 ns |  1.74 ns |  1.54 ns |
|            'Deserialize from Csv' |   703.0 ns |  5.01 ns |  4.69 ns |
|     'Serialize to NewtonsoftJson' |   574.5 ns |  8.07 ns |  7.15 ns |
| 'Deserialize from NewtonsoftJson' | 1,134.5 ns |  7.12 ns |  6.66 ns |
|     'Serialize to SystemTextJson' |   243.0 ns |  2.85 ns |  2.67 ns |
| 'Deserialize from SystemTextJson' | 1,176.8 ns | 22.67 ns | 20.10 ns |
